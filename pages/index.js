//import Header from './comps/Header';
//import withLayout from './comps/MyLayout';
import Layout from './comps/MyLayout';

// export default function Index() {
//   return (
//     <Layout>
//       <p>Hello Next.js</p>
//     </Layout>
//   );
// }
//Method 1*****
// const Page = () => <p>Hello Next.js</p>;

// export default withLayout(Page);

//Methode 2*****
const IndexPageContent = <p>Hello Next.js</p>;

export default function Index(){
    return <Layout content={IndexPageContent} />;
}