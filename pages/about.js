//import Header from './comps/Header';
//import Link from 'next/link';
// import withLayout from './comps/MyLayout';

import Layout from './comps/MyLayout';

// export default function About() {
//     return (
//     //   <div>
//     //       <Header />
//     //     <p>This is the about page</p>
//     //   </div>
//     <Layout>
//         <p>This is the about page</p>
//     </Layout>
//     );
//   }

//Method 1****
// const Page = () => <p>This is the about page</p>;

// export default withLayout(Page);

//Method 2*****

const aboutPageContent= <p>This is the about page Method 2</p>;

export default function About(){
    return <Layout content= {aboutPageContent} />;
}